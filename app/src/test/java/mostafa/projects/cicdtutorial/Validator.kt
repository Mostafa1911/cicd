package mostafa.projects.cicdtutorial

import com.google.common.truth.Truth
import mostafa.projects.cicdtutorial.ValidatorInputs.fullNameValid
import mostafa.projects.cicdtutorial.ValidatorInputs.phoneValid
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/*
  Mostafa Gad 30 / 5/ 2022
  unit test to test validation of auth module inputs

 */
@RunWith(JUnit4::class)
class Validator {

    @Test
    fun validateFullName(){
        var name = "Mostafa"
        var nameValid = name.fullNameValid()
        Truth.assertThat(nameValid).isTrue()
    }

    @Test
    fun validatePhone(){
        var phone = "0102357325"
        var phoneValid = phone.phoneValid()
        Truth.assertThat(phoneValid).isTrue()
    }

}